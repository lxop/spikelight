/** @file battery.c
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Module for monitoring the battery level.
 */

#include <avr/interrupt.h>
#include <avr/power.h>
#include "main.h"
#include "battery.h"
#include "lights.h"

/* The strategy here is thus:
 *   Probably only need to sample the battery level once per second 
 *   (say), so power it down in between to save juice.  But the 
 *   bandgap reference apparently needs 1ms to stabilise after the
 *   ADC is enabled, and I don't want to go sitting in a busy loop 
 *   waiting for it to come right.  So the first time through the
 *   battery_update() function, the ADC is enabled to give the
 *   bandgap time to settle.  But if the chip is put to sleep while
 *   the ADC is enabled, a conversion starts automatically.  So I
 *   disable the interrupt for this conversion, so that the chip
 *   doesn't wake up and try to do anything with this dud result.
 *   Then the next time through the function, enable the interrupt
 *   so that the next sample is dealt with.  The measurement will be
 *   performed, and the ISR will store the value and disable the ADC.
 *   The third time through the function, the new value is stored into
 *   the ring buffer, then the stored values are averaged in the fourth
 *   pass through the function.  Finally, in the fifth pass, functions
 *   are called to either limit or un-limit power usage in the UI.
 *   Then the function does nothing for the remainder of the second
 *   (or whatever the sampling period is chosen to be), and then starts
 *   the cycle again.
 * 
 * Note that this actually measures 1.1V / Vcc, so the values are
 * inverted (a lower reading corresponds to a higher voltage).  But 
 * that's fine.
 */

#define BATTERY_POLL_TICKS  AS_TICKS(BATTERY_POLL_MS)

/* The FSM state. */
static enum 
{
	WAIT,
	ENABLE_ADC, 
	START_CONVERSION, 
	READ_VALUE, 
	CALC_AVERAGE,
	USE_RESULT
} adc_tick_mode = WAIT;

static TYPE_TO_HOLD (BATTERY_POLL_TICKS) loop_ticks = 0;

/* Mild trickery to easily access the individual bytes. */
static volatile union 
{
	uint16_t value;
	struct {
		/* The ordering of these is correct on my x86_64 machine,
		 * and appears to be correct in the Atmel simulator. */
		uint8_t low;
		uint8_t high;
	} bytes;
} last_reading;

static uint16_t recent_Vccs [N_VCCS];
static uint8_t vcc_idx = 0;
static uint16_t battery_level;

void battery_init (void)
{
	/* Setup the ADC */
	// Set prescaler to give clock of ~100kHz
#if F_CPU == 1000000
	// 1MHz, prescaler of 16 -> ADC clock of 62.5kHz
	setbit (ADCSRA, ADPS2);
#elif F_CPU == 128000
	// 128kHz, prescaler of 2 -> ADC clock of 64kHz
	setbit (ADCSRA, ADPS0);
#else
	#error Unsupported clock frequency
#endif
	// Voltage reference is Vcc - this is default
	// Measured value is Vbg (bandgap)
	setbit (ADMUX, MUX3);
	setbit (ADMUX, MUX2);
}

static inline void enable_adc (void)
{
	setbit (ADCSRA, ADEN);
}

static inline void disable_adc (void)
{
	clrbit (ADCSRA, ADEN);
}

static uint16_t average_Vcc (void)
{
	uint16_t sum = 0;
	for (uint8_t i = 0; i < N_VCCS; i++)
	{
		sum += recent_Vccs [i];
	}
	return sum / N_VCCS;
}

void battery_tick (void)
{
	loop_ticks++;
	switch (adc_tick_mode)
	{
		case WAIT:
			if (loop_ticks >= BATTERY_POLL_TICKS)
			{
				adc_tick_mode = ENABLE_ADC;
				loop_ticks = 0;
			}
			break;
		case ENABLE_ADC:
			power_adc_enable ();
			enable_adc ();
			clrbit (ADCSRA, ADIE);
			adc_tick_mode = START_CONVERSION;
			break;
		case START_CONVERSION:
			setbit (ADCSRA, ADIE);
			adc_tick_mode = READ_VALUE;
			break;
		case READ_VALUE:
			/* This sort of thing would usually require syncronisation
			 * because of the multi-byte nature of last_reading.value,
			 * but since it is only written in the ADC ISR, which only
			 * runs once per second (controlled by this state machine),
			 * there isn't any way for a race condition to occur. */
			recent_Vccs[vcc_idx] = last_reading.value;
			// Can use % because N_VCCS is 2^x
			vcc_idx = (vcc_idx + 1) % N_VCCS; 
			adc_tick_mode = CALC_AVERAGE;
			break;
		case CALC_AVERAGE:
			battery_level = average_Vcc ();
			adc_tick_mode = USE_RESULT;
			break;
		case USE_RESULT:
			if (battery_level > LOW_BATTERY_THRESHOLD)
				disable_high_power ();
			else if (battery_level < GOOD_BATTERY_THRESHOLD)
				enable_high_power ();
			
			adc_tick_mode = WAIT;
			break;
	}
}

/* Just copy the value into memory and disable the ADC.  Processing
 * happens in the main tick loop. */
ISR (ADC_vect)
{
	// Copy the value into memory
	last_reading.bytes.low = ADCL;
	last_reading.bytes.high = ADCH;
	// Power down the ADC to save energy
	disable_adc ();
	power_adc_disable ();
}

/* Returns a value in [1..5] according to the battery level.
 * 1 is 0--20%,
 * 5 is 80--100% */
uint8_t get_battery_level (void)
{
	if (battery_level > LEVEL_20)
		return 1;
	if (battery_level > LEVEL_40)
		return 2;
	if (battery_level > LEVEL_60)
		return 3;
	if (battery_level > LEVEL_80)
		return 4;
	return 5;
}
