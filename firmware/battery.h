/** @file battery.h
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Module for monitoring the battery level.
 */

#ifndef BATTERY_H
#define BATTERY_H

/* How often to measure the battery level, in milliseconds */
#define BATTERY_POLL_MS  1000

/* How many recent values to average across.  A power of two will
 * make for much faster code. */
#define N_VCCS  4

/* If the ADC reads above the low threshold, then the battery level is
 * considered to be low, and high power modes should be disabled. 
 * The battery level is then considered to be low unless the reading
 * gets below the good battery threshold.  These are split to 
 * introduce some hysteresis, and thus prevent any flickering of the
 * light due to noise in the readings when the battery is at the
 * threshold level.
 * 
 * Reference values: 2.75V -> 410   (102) (8 bit reading)
 *                   2.8V  -> 402   (101)
 *                   2.9V  -> 388   (97)
 *                   2.95V -> 382   (95)
 *                   3.0V  -> 375   (94)
 *                   3.1V  -> 363   (91)
 *                   3.15V -> 358   (89)
 *                   3.2V  -> 352   (88)
 *                   3.3V  -> 341   (85)
 *                   3.35V -> 336   (84)
 *                   3.4V  -> 331   (82)
 *                   3.5V  -> 322   (80)
 *                   3.55V -> 317   (79)
 *                   3.6V  -> 314   (78)
 *                   3.65V -> 308   (77)
 *                   3.75V -> 300   (75)
 * TODO: calibrate these values. */
#define LOW_BATTERY_THRESHOLD   375
#define GOOD_BATTERY_THRESHOLD  363

#define LEVEL_80  317
#define LEVEL_60  336
#define LEVEL_40  358
#define LEVEL_20  382

void battery_init (void);
void battery_tick (void);

uint8_t get_battery_level (void);

/* These need to be implemented in the UI code. */
extern void disable_high_power (void);
extern void enable_high_power (void);

#endif /* BATTERY_H */
