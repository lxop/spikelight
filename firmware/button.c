/** @file button.c
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief
 */

#include "main.h"
#include "button.h"

/* Convert timing values into ticks */
#define DEBOUNCE_TICKS      AS_TICKS(DEBOUNCE_MS)
#define DOUBLE_CLICK_TICKS  AS_TICKS(DOUBLE_CLICK_MS)
#define HOLD_TICKS          AS_TICKS(HOLD_MS)

static enum 
{
	WAITING, 
	FIRST_PRESS_DEBOUNCE, 
	PRESSED, 
	RELEASE_DEBOUNCE, 
	RELEASED, 
	SECOND_PRESS_DEBOUNCE,
	HELD,
	FINAL_RELEASE_DEBOUNCE
} state = WAITING;

static TYPE_TO_HOLD(HOLD_TICKS) time_in_state;

void button_init (void)
{
	/* Set button pin as an input */
	clrbit (DDRB, PIN_BUTTON);
	
	/* Enable pull-up */
	clrbit (MCUCR, PUD); /* Ensure pull-up is not disabled globally */
	setbit (PORTB, PIN_BUTTON);
}

/* Allow up to 250 ms from first press to second press for double
 * click, after that time read the input as a single click.
 * 
 * 10 ms seems reasonable for a debounce time, but I haven't had an
 * opportunity to experiment with the reed switch to confirm.
 */

/* Called every TICK_PERIOD_MS */
click_t button_check (void)
{
	click_t ret = NO_CLICK;
	
	/* Read the button value - goes to zero when pressed. */
	uint8_t this_reading = getbit (PINB, PIN_BUTTON);
	
	switch (state)
	{
		case WAITING:
			/* This is the normal case, button not pressed, so just
			 * check whether the button has been pressed this time.
			 */
			if (!this_reading)
			{
				state = FIRST_PRESS_DEBOUNCE;
				time_in_state = 0; // Or 1? does it matter much?
			}
			break;
		
		case FIRST_PRESS_DEBOUNCE:
			/* Just wait for a predetermined time to ignore
			 * the state of the button while it might be bouncing.
			 */
			time_in_state++;
			if (time_in_state >= DEBOUNCE_TICKS)
			{
				time_in_state = 0;
				state = PRESSED;
			}
			break;
		
		case PRESSED:
			/* If we stay in this state for the hold time, then read it
			 * as a hold, otherwise just wait for the release.  A single
			 * click will be registered from the RELEASED state.
			 */
			if (this_reading)
			{
				/* Button was released */
				time_in_state = 0;
				state = RELEASE_DEBOUNCE;
			}
			else
			{
				/* Button is still held down */
				time_in_state++;
				if (time_in_state >= HOLD_TICKS)
				{
					//time_in_state = 0; // not needed
					state = HELD;
					ret = LONG_PRESS;
				}
			}
			break;
			
		case RELEASE_DEBOUNCE:
			/* Just wait for a predetermined time to ignore
			 * the state of the button while it might be bouncing.
			 */
			time_in_state++;
			if (time_in_state >= DEBOUNCE_TICKS)
			{
				time_in_state = 0;
				state = RELEASED;
			}
			break;
		
		case RELEASED:
			/* Check the button for a second press within a preset
			 * time.  If no further press is noticed, return a single
			 * click.
			 */
			if (!this_reading)
			{
				/* Button was pressed again */
				time_in_state = 0;
				state = SECOND_PRESS_DEBOUNCE;
			}
			else
			{
				/* Button has not been pressed */
				time_in_state++;
				if (time_in_state >= DOUBLE_CLICK_TICKS)
				{
					time_in_state = 0;
					state = WAITING;
					ret = SINGLE_CLICK;
				}
			}
			break;
		
		case SECOND_PRESS_DEBOUNCE:
			/* Just wait for a predetermined time to ignore
			 * the state of the button while it might be bouncing.
			 * After the debounce, return a double click.
			 */
			time_in_state++;
			if (time_in_state >= DEBOUNCE_TICKS)
			{
				//time_in_state = 0; // not needed
				state = HELD;
				ret = DOUBLE_CLICK;
			}
			break;
		
		case HELD:
			/* Just a holding state, really.  Wait for the button to
			 * be released.  Doesn't matter how long it is held, so
			 * don't need to keep time.  The double click was already
			 * registered at the end of the debounce above.
			 */
			if (this_reading)
			{
				/* The button has been released. */
				time_in_state = 0;
				state = FINAL_RELEASE_DEBOUNCE;
			}
			break;
		
		case FINAL_RELEASE_DEBOUNCE:
			/* Just wait for a predetermined time to ignore
			 * the state of the button while it might be bouncing.
			 */
			time_in_state++;
			if (time_in_state >= DEBOUNCE_TICKS)
			{
				//time_in_state = 0; //not needed
				state = WAITING;
			}
			break;
		
	}
	return ret;
}

