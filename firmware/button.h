/** @file button.h
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief
 */

#ifndef BUTTON_H
#define BUTTON_H

// Timing values for button control
#define DEBOUNCE_MS 50
#define HOLD_MS 1000
#define DOUBLE_CLICK_MS 300

typedef enum {NO_CLICK, SINGLE_CLICK, DOUBLE_CLICK, LONG_PRESS} click_t;

void button_init (void);
click_t button_check (void);

#endif /* BUTTON_H */
