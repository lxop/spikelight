#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 1000000UL
#include <util/delay.h>
#include <util/atomic.h>
#include <avr/sleep.h>
#include <string.h>
#include <avr/cpufunc.h>

/* Intention: read the ADC, and output the value on PORTB.
 * PortB is only 6 wide, while the readings are 10 bits wide.
 * So could use 5 bits for data plus one for indicating the timing of
 * the MSB?  This would require the scope to have 6 channels...
 * Alternatively, could bit bash it all out on one pin.  That's
 * probably easier.
 */

#define SLEEP_TIME_MS 10

/* Bit manipulation macros */
#define bit(index)        (1<<(index))
#define setbit(port,pin)  (port |= bit(pin))
#define clrbit(port,pin)  (port &= ~(bit(pin)))
#define toggle(port,pin)  (port ^= bit(pin))
#define getbit(port,pin)  (port & bit(pin))

#define NUM_BUFFER_BYTES 128

struct buffer_type
{
  uint8_t buf[NUM_BUFFER_BYTES];
  uint8_t curW;
  uint8_t curR;
  uint8_t len;
};

static struct buffer_type tx;
//static struct buffer_type rx;

void init_spi (void)
{
	/* Set port directions */
	setbit (DDRB, 1);
	/* 3-wire mode */
	setbit (USICR, USIWM0);
	/* Clock from the master node */
	setbit (USICR, USICS1);
	/* Enable tx complete interrupt */
	setbit (USICR, USIOIE);
	/* Put sync pattern into the buffer */
	USIDR = 0xB4;
	memset (tx.buf, 0, NUM_BUFFER_BYTES);
}

uint8_t spi_put(uint8_t *b, uint8_t n)
{
	uint8_t i = 0;

	/* All or nothing */
	if (tx.len + n < NUM_BUFFER_BYTES)
	{
		for (i = 0; i < n; i++)
		{
			tx.buf[tx.curW] = b[i];
			tx.curW = (tx.curW + 1) % NUM_BUFFER_BYTES;
		}
		ATOMIC_BLOCK (ATOMIC_FORCEON)
		{
			tx.len += n;
		}
	}
	else
	{
		tx.buf[tx.curW] = 0x21;
	}

	// Return number of bytes added to tx buffer.
	return i;
}

/*
uint8_t spi_get(uint8_t *b, uint8_t n)
{
  uint8_t i;

  i = 0;

  cli();

  while (rx.len && i < n)
  {
    b[i] = rx.buf[rx.cur];
    rx.cur = (rx.cur + 1) % NUM_BUFFER_BYTES;
    --rx.len;
    ++i;
  }

  sei();

  return i;
}
*/

void init_adc (void)
{
	/* Vcc reference (default) */
	/* Vbg input */
	setbit (ADMUX, MUX3); 
	setbit (ADMUX, MUX2);
	/* Right adjusted result (default) */
	/* Enable ADC */
	setbit (ADCSRA, ADEN);
	/* Enable ADC interrupt */
	setbit (ADCSRA, ADIE);
	/* ADC prescaler 16 */
	setbit (ADCSRA, ADPS2);
}

int main (void)
{
	init_adc();
	init_spi();
	
	sei ();
	sleep_enable();
	while (1)
	{
		sleep_cpu();
		_NOP();
		_NOP();
		_NOP();
		_NOP();
		_NOP();
		_NOP();
	}
	return 0;
}



ISR (ADC_vect, ISR_NOBLOCK)
{
	/* Read the ADC value */
	uint8_t adc_val[2] = {ADCH, ADCL};
	//uint8_t adc_val[2] = {0xAB, 0xCD};
	/* Write it down the SPI channel */
	spi_put (adc_val, 2);
}

ISR (USI_OVF_vect, ISR_BLOCK)
{
	/* Clear the interrupt flag */
	setbit (USISR, USIOIF);
	// If byte in USIDR is non zero and space is available in the 
	// rx queue, read byte from USIDR and place in rx queue.
	//if (USIDR && rx.len < NUM_BUFFER_BYTES)
	//{
		//rx.buf[(rx.cur + rx.len) % NUM_BUFFER_BYTES] = USIDR;
		//rx.len++;
	//}

	// If bytes are pending in the tx queue place in USIDR.
	if (tx.len)
	{
		USIDR = tx.buf[tx.curR];
		tx.curR = (tx.curR + 1) % NUM_BUFFER_BYTES;
		tx.len--;
	}
	// Otherwise clear USIDR.
	else
		USIDR = 0;
}
