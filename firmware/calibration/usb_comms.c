#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <usb.h>
#include "usbtiny.h"

#define CLK_PERIOD 0.00025  // In seconds


// Wrapper for simple usb_control_msg messages
int usb_control (usb_dev_handle* h,
			unsigned int requestid, unsigned int val, unsigned int index )
{
  int nbytes;
  nbytes = usb_control_msg(h,
			    USB_ENDPOINT_IN | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
			    requestid,
			    val, index,           // 2 bytes each of data
			    NULL, 0,              // no data buffer in control messge
			    USB_TIMEOUT );        // default timeout
  if(nbytes < 0){
    fprintf(stderr, "\nerror: usbtiny_transmit: %s\n", usb_strerror());
    return -1;
  }

  return nbytes;
}

// Wrapper for simple usb_control_msg messages to receive data from programmer
int usb_in (usb_dev_handle* h,
		   unsigned int requestid, unsigned int val, unsigned int index,
		   unsigned char* buffer, int buflen, int bitclk )
{
  int nbytes;
  int timeout;
  int i;

  // calculate the amout of time we expect the process to take by
  // figuring the bit-clock time and buffer size and adding to the standard USB timeout.
  timeout = USB_TIMEOUT + (buflen * bitclk) / 1000;

  for (i = 0; i < 10; i++) {
    nbytes = usb_control_msg(h,
			      USB_ENDPOINT_IN | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
			      requestid,
			      val, index,
			      (char *)buffer, buflen,
			      timeout);
    if (nbytes == buflen) {
      return nbytes;
    }
  }
  fprintf(stderr, "\nerror: usbtiny_receive: %s (expected %d, got %d)\n",
          usb_strerror(), buflen, nbytes);
  return -1;
}


// Wrapper for simple usb_control_msg messages to send data to programmer
int usb_out (usb_dev_handle* h,
		    unsigned int requestid, unsigned int val, unsigned int index,
		    unsigned char* buffer, int buflen, int bitclk )
{
  int nbytes;
  int timeout;

  // calculate the amout of time we expect the process to take by
  // figuring the bit-clock time and buffer size and adding to the standard USB timeout.
  timeout = USB_TIMEOUT + (buflen * bitclk) / 1000;

  nbytes = usb_control_msg( h,
			    USB_ENDPOINT_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
			    requestid,
			    val, index,
			    (char *)buffer, buflen,
			    timeout);
  if (nbytes != buflen) {
    fprintf(stderr, "\nerror: usbtiny_send: %s (expected %d, got %d)\n",
	    usb_strerror(), buflen, nbytes);
    return -1;
  }

  return nbytes;
}

/* Find a device with the correct VID/PID match for USBtiny */

int usbtiny_open(usb_dev_handle** h)
{
  struct usb_bus      *bus;
  struct usb_device   *dev = 0;
  int vid, pid;
  
  *h = NULL;

  usb_init();                    // initialize the libusb system
  usb_find_busses();             // have libusb scan all the usb busses available
  usb_find_devices();            // have libusb scan all the usb devices available

  vid = USBTINY_VENDOR_DEFAULT;
  pid = USBTINY_PRODUCT_DEFAULT;
  
  // now we iterate through all the busses and devices
  for ( bus = usb_busses; bus; bus = bus->next ) 
  {
    for	( dev = bus->devices; dev; dev = dev->next ) 
    {
      if (dev->descriptor.idVendor == vid
	  && dev->descriptor.idProduct == pid ) 
	  {   // found match?
		fprintf(stderr,
	      "usbdev_open(): Found USBtinyISP, bus:device: %s:%s\n",
	      bus->dirname, dev->filename);
		*h = usb_open(dev);           // attempt to connect to device

		// wrong permissions or something?
		if (!h) {
			fprintf(stderr, "Warning: cannot open USB device: %s\n",
				usb_strerror());
			continue;
		}
      }
    }
  }

  if (!*h) {
    fprintf( stderr, "Error: Could not find USBtiny device (0x%x/0x%x)\n",
	     vid, pid );
    return -1;
  }

  return 0;                  // If we got here, we must have found a good USB device
}

void usbtiny_close (usb_dev_handle* h)
{
    if (h)
	usb_close(h);
}

/* Given a SCK bit-clock speed (in seconds) we verify its an OK speed and tell the
   USBtiny to update itself to the new frequency */
int usbtiny_set_sck_period (usb_dev_handle* h, double T)
{
  int sck_period = (int)(T * 1e6 + 0.5);   // convert from s to us, 'int', the 0.5 is for rounding up

  // Make sure it's not 0, as that will confuse the usbtiny
  if (sck_period < SCK_MIN)
    sck_period = SCK_MIN;

  // We can't go slower, due to the byte-size of the clock variable
  if  (sck_period > SCK_MAX)
    sck_period = SCK_MAX;

  // send the command to the usbtiny device.
  if (usb_control(h, USBTINY_POWERUP, sck_period, RESET_LOW) < 0)
    return -1;
  usleep (100000);
  if (usb_control(h, USBTINY_POWERUP, sck_period, RESET_HIGH) < 0)
    return -1;

  // with the new speed, we'll have to update how much data we send per usb transfer
  //usbtiny_set_chunk_size(pgm, PDATA(pgm)->sck_period);
  return 0;
}

/* Tell the USBtiny to release the output pins, etc */
void usbtiny_powerdown(usb_dev_handle* h)
{
  if (!h) {
    return;                 // wasn't connected in the first place
  }
  usb_control(h, USBTINY_POWERDOWN, 0, 0);      // Send USB control command to device
}

/* Send a 4-byte SPI command to the USBtinyISP for execution */
int usbtiny_spi(usb_dev_handle* h, unsigned char mosi[4], unsigned char miso[4], double sck_period)
{
  int nbytes;

  // Make sure its empty so we don't read previous calls if it fails
  memset(miso, '\0', 4);

  nbytes = usb_in(h,  USBTINY_SPI,
		   (mosi[1] << 8) | mosi[0],  // convert to 16-bit words
		   (mosi[3] << 8) | mosi[2],  //  "
			miso, 4, 8 * sck_period );

#if 0
  // print out the data we sent and received
  printf( "CMD: [%02x %02x %02x %02x] [%02x %02x %02x %02x]\n",
      cmd[0], cmd[1], cmd[2], cmd[3],
      res[0], res[1], res[2], res[3] );
#endif

  return (nbytes == 4);     // should have read 4 bytes
}

void print_bytes (unsigned char* x, int num)
{
    int i;
    for (i = 0; i < num; i++)
    {
	//if (isgraph (x[i]))
	    //printf ("%c ", x[i]);
	//else
	    printf ("0x%02X ", x[i]);
    }
}


int main (int argc, char* argv[])
{
    usb_dev_handle* h;
    int ok, i;
    double clk_period;
    unsigned char buf_out[4];
    unsigned char buf_in[4];
    memset(buf_out, 0, 4);
    
    if (argc > 1)
    {
	if (sscanf (argv[1], "%lf", &clk_period) != 1)
	{
	    printf ("Failed to parse input clock period, using default\n");
	    clk_period = CLK_PERIOD;
	}
    }
    else
	clk_period = CLK_PERIOD;
    
    if (usbtiny_open (&h) < 0)
	return 2;
    
    usbtiny_set_sck_period(h, clk_period);
    // Let the device wake up
    usleep (100000);
    
    //byte_count = usb_in (h, USBTINY_SPI, 0, 0, buf, 4, SCK_DEFAULT);
    for (i = 0; i < 40; i++)
    {
	ok = usbtiny_spi (h, buf_out, buf_in, clk_period);
	if (ok)
	{
	    printf ("Buffer: ");
	    print_bytes (buf_in, 4);
	    printf ("\n");
	}
	else
	{
	    printf ("Didn't read enough bytes!\n");
	}
    }
    usbtiny_powerdown(h);
    usbtiny_close(h);
    return 0;
}

/* So, ideally I would have functions
 *   set_clock_rate
 *   send_byte
 *   recv_byte
 * and that's about it, maybe also a startup function if necessary.
 * The send and recv functions should only need the data to send
 * or a buffer to fill.
 */
