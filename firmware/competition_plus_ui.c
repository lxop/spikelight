/** @file competition_plus_ui.c
 *  @date February 2014
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Defines all the functions controlling the competition plus UI.
 */

#include "modes.h"
#include "main.h"
#include "lights.h"
#include "battery.h"

#define N_NORMAL_LEVELS    5
#define N_POWERSAVE_LEVELS 4

#define NORMAL_SPOT_BRIGHTNESS     100
#define POWERSAVE_SPOT_BRIGHTNESS  50

typedef enum {SPOT, FLOOD} competition_mode_output_t;
static uint8_t normal_levels    [N_NORMAL_LEVELS]    = {1, 5, 15, 40, 100};
static uint8_t powersave_levels [N_POWERSAVE_LEVELS] = {1, 5, 15, 40};
static uint8_t *levels = normal_levels;
static uint8_t max_levels = N_NORMAL_LEVELS;

/* State variables */
competition_mode_output_t output = FLOOD;
static uint8_t brightness_index = 2; // Start with 15%

static uint8_t spot_brightness = NORMAL_SPOT_BRIGHTNESS;
static bool_t in_powersave_mode = FALSE;

static mode_t off_mode;
static mode_t competition_mode;
static mode_t programming_mode;
static mode_t sos_pattern_mode;
static mode_t battery_indicator_mode;
static mode_t everything_on_mode;
static mode_t strobe_mode;
static mode_t lock_mode;

static void off_enter (void);
static void competition_enter (void);
static void programming_enter (void);
static void sos_enter (void);
static void battery_enter (void);
static void everything_enter (void);
static void strobe_enter (void);
static void lock_enter (void);

mode_t* ui_init (void)
{
	off_enter ();
	return &off_mode;
}

static void do_nothing (void) {}

/**********************************************************
 * Off mode - no lights on, Long press to enter 
 * competition mode.
 **********************************************************/
static void off_enter (void)
{
	floods_off ();
	spot_off ();
}

static mode_t* off_nop (void)
{
	return &off_mode;
}

static mode_t* off_to_competition_mode (void)
{
	competition_enter ();
	return &competition_mode;
}

static mode_t* off_to_extra_mode (void)
{
	sos_enter ();
	return &sos_pattern_mode;
}

static mode_t off_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        off_nop,
	/* on_double_click = */ off_to_extra_mode,
	/* on_hold = */         off_to_competition_mode
};


/**********************************************************
 * Competition mode - toggles between flood and spotlight,
 * a double click goes to programming mode, and a hold
 * switches it off.
 **********************************************************/
static void competition_enter (void)
{
	if (output == FLOOD)
	{
		spot_off ();
		set_floods_brightness (levels [brightness_index]);
	}
	else /* output == SPOT */
	{
		floods_off ();
		set_spot_brightness (spot_brightness);
	}
}
static mode_t* competition_toggle (void)
{
	if (output == FLOOD)
	{
		output = SPOT;
		floods_off ();
		set_spot_brightness (spot_brightness);
	}
	else /* output == SPOT */
	{
		output = FLOOD;
		spot_off ();
		set_floods_brightness (levels [brightness_index]);
	}
	return &competition_mode;
}
static mode_t* competition_to_programming (void)
{
	programming_enter ();
	return &programming_mode;
}
static mode_t* competition_to_off (void)
{
	off_enter ();
	return &off_mode;
}
static mode_t competition_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        competition_toggle,
	/* on_double_click = */ competition_to_programming,
	/* on_hold = */         competition_to_off
};



/**********************************************************
 * Programming mode - alters the brightness of the floods
 * (so switches to flood mode if not there already), a
 * double click goes back to competition mode, and a hold
 * switches it off.
 **********************************************************/
static void increment_flood_brightness (void)
{
	brightness_index += 1;
	if (brightness_index >= max_levels)
		brightness_index = 0;
	set_floods_brightness (levels [brightness_index]);
}
static void programming_enter (void)
{
	output = FLOOD;
	spot_off ();
	set_floods_brightness (levels [brightness_index]);
}
static mode_t* programming_to_off (void)
{
	off_enter ();
	return &off_mode;
}
static mode_t* programming_increment (void)
{
	increment_flood_brightness ();
	return &programming_mode;
}
static mode_t* programming_to_competition (void)
{
	return &competition_mode;
}
static mode_t programming_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        programming_increment,
	/* on_double_click = */ programming_to_competition,
	/* on_hold = */         programming_to_off
};



/**********************************************************
 * Extra mode - click rotates between SOS, battery,
 * everything 100%, fast strobe, medium strobe, slow
 * strobe.  Double click goes to lock mode.  Hold 
 * switches off.
 **********************************************************/
// Extra mode is actually implemented with a set of modes
static mode_t* extra_to_off (void)
{
	off_enter ();
	return &off_mode;
}
static mode_t* extra_to_lock (void)
{
	lock_enter ();
	return &lock_mode;
}

/** SOS pattern */
#define SOS_DIT_MS      200
#define SOS_DAH_DITS    3   // How many dits in a dah
#define SOS_SPACE_DITS  7   // How many dits in the gap between transmissions
#define SOS_DIT_TICKS   AS_TICKS(SOS_DIT_MS)
#define SOS_BRIGHTNESS  100  // Full brightness
#define SOS_DITS_MAX    MAX(3*SOS_DAH_DITS + 3, SOS_SPACE_DITS)
static TYPE_TO_HOLD(SOS_DIT_TICKS) sos_ticks = 0;
static TYPE_TO_HOLD(SOS_DITS_MAX) sos_dits = 0;
static enum {S1, OH, S2, SPACE} sos_state;
static mode_t* sos_to_battery (void)
{
	battery_enter ();
	return &battery_indicator_mode;
}
static void sos_enter (void)
{
	sos_ticks = 0;
	sos_dits = 0;
	sos_state = S1;
	floods_off ();
}
static void sos_update (void)
{
	sos_ticks++;
	if (sos_ticks >= SOS_DIT_TICKS)
	{
		sos_ticks = 0;
		sos_dits++;
		switch (sos_state)
		{
			case S1:
			case S2:
				if (sos_dits == 1 || sos_dits == 3 || sos_dits == 5)
				{
					set_floods_brightness (SOS_BRIGHTNESS);
				}
				else if (sos_dits == 2 || sos_dits == 4)
				{
					floods_off ();
				}
				else if (sos_dits == 6)
				{
					floods_off ();
					sos_dits = 0;
					/* Don't specify explicitly since this is used by both
					 * S states. */
					sos_state++;
				}
				break;
			case OH:
				if (sos_dits ==                   1  ||
					sos_dits == (  SOS_DAH_DITS + 2) ||
					sos_dits == (2*SOS_DAH_DITS + 3))
				{
					set_floods_brightness (SOS_BRIGHTNESS);
				}
				else if (sos_dits == (  SOS_DAH_DITS + 1) ||
						 sos_dits == (2*SOS_DAH_DITS + 2))
				{
					floods_off ();
				}
				else if (sos_dits == (3*SOS_DAH_DITS + 3))
				{
					floods_off ();
					sos_dits = 0;
					sos_state = S2;
				}
				break;
			case SPACE:
				if (sos_dits >= SOS_SPACE_DITS)
				{
					sos_dits = 0;
					sos_state = S1;
				}
				break;
		}
	}
}
static mode_t sos_pattern_mode =
{
	/* update = */   sos_update,
	
	/* on_click = */        sos_to_battery,
	/* on_double_click = */ extra_to_lock,
	/* on_hold = */         extra_to_off
};

/** Battery indicator mode */
#define BATTERY_BRIGHTNESS       15
#define BATTERY_FLASH_ON_MS      60
#define BATTERY_FLASH_OFF_MS     440
#define BATTERY_FLASH_PERIOD_MS  (BATTERY_FLASH_ON_MS + BATTERY_FLASH_OFF_MS)
#define BATTERY_GAP_MS           2000
#define BATTERY_FLASH_OFF_TICKS     AS_TICKS(BATTERY_FLASH_OFF_MS)
#define BATTERY_FLASH_PERIOD_TICKS  AS_TICKS(BATTERY_FLASH_PERIOD_MS)
#define BATTERY_GAP_TICKS           AS_TICKS(BATTERY_GAP_MS)
#define BATTERY_TICKS_MAX           (BATTERY_FLASH_PERIOD_TICKS + BATTERY_GAP_TICKS)
static TYPE_TO_HOLD(BATTERY_TICKS_MAX) battery_ticks = 0;
static uint8_t battery_flashes_done = 0;
static uint8_t battery_flashes_needed = 0;
static void battery_enter (void)
{
	// Switch off
	floods_off ();
	// Start by waiting for half of the gap time
	battery_ticks = BATTERY_TICKS_MAX - BATTERY_GAP_TICKS/2;
}
static void battery_update (void)
{
	battery_ticks++;
	if (battery_ticks == BATTERY_FLASH_OFF_TICKS)
	{
		set_floods_brightness (BATTERY_BRIGHTNESS);
	}
	else if (battery_ticks == BATTERY_FLASH_PERIOD_TICKS)
	{
		battery_flashes_done++;
		floods_off ();
		if (battery_flashes_done < battery_flashes_needed)
		{
			battery_ticks = 0;
		}
	}
	else if (battery_ticks >= BATTERY_TICKS_MAX)
	{
		battery_ticks = 0;
		battery_flashes_done = 0;
		battery_flashes_needed = get_battery_level ();
	}
}
// Skip everything 100% mode if we are in powersave mode
static mode_t* battery_to_next (void)
{
	if (in_powersave_mode)
	{
		strobe_enter ();
		return &strobe_mode;
	}
	else
	{
		everything_enter ();
		return &everything_on_mode;
	}
}
static mode_t battery_indicator_mode =
{
	/* update = */   battery_update,
	
	/* on_click = */        battery_to_next,
	/* on_double_click = */ extra_to_lock,
	/* on_hold = */         extra_to_off
};

/** Everything on 100% */
static void everything_enter (void)
{
	set_floods_brightness (100);
	set_spot_brightness (100);
}
static mode_t* everything_to_strobe (void)
{
	strobe_enter ();
	return &strobe_mode;
}
static mode_t everything_on_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        everything_to_strobe,
	/* on_double_click = */ extra_to_lock,
	/* on_hold = */         extra_to_off
};

/** Strobe modes (fast, medium, slow) */
typedef enum {STROBE_FAST, STROBE_MEDIUM, STROBE_SLOW} strobe_speed_t;
static strobe_speed_t strobe_speed = STROBE_FAST;
#define STROBE_BRIGHTNESS_NORMAL     100
#define STROBE_BRIGHTNESS_POWERSAVE  50
static uint8_t strobe_brightness = STROBE_BRIGHTNESS_NORMAL;
#define STROBE_FAST_ON_TIME_MS    100
#define STROBE_MEDIUM_ON_TIME_MS  250
#define STROBE_SLOW_ON_TIME_MS    500
#define STROBE_FAST_ON_TIME_TICKS    AS_TICKS(STROBE_FAST_ON_TIME_MS)
#define STROBE_MEDIUM_ON_TIME_TICKS  AS_TICKS(STROBE_MEDIUM_ON_TIME_MS)
#define STROBE_SLOW_ON_TIME_TICKS    AS_TICKS(STROBE_SLOW_ON_TIME_MS)
#define STROBE_MAX_TICKS    (2*STROBE_SLOW_ON_TIME_TICKS)
static TYPE_TO_HOLD(STROBE_MAX_TICKS) strobe_ticks = 0;
static typeof(strobe_ticks) strobe_max;
static void strobe_enter (void)
{
	strobe_speed = STROBE_FAST;
	strobe_max = STROBE_FAST_ON_TIME_TICKS;
	strobe_ticks = 0;
	set_floods_brightness (strobe_brightness);
	spot_off ();
}
static mode_t* strobe_to_next (void)
{
	switch (strobe_speed)
	{
		case STROBE_FAST:
			strobe_speed = STROBE_MEDIUM;
			strobe_max = STROBE_MEDIUM_ON_TIME_TICKS;
			break;
		case STROBE_MEDIUM:
			strobe_speed = STROBE_SLOW;
			strobe_max = STROBE_SLOW_ON_TIME_TICKS;
			break;
		case STROBE_SLOW:
			sos_enter ();
			return &sos_pattern_mode;
	}
	strobe_ticks = 0;
	set_floods_brightness (strobe_brightness);
	return &strobe_mode;
}
static void strobe_update (void)
{
	strobe_ticks++;
	if (strobe_ticks == strobe_max)
	{
		floods_off ();
	}
	else if (strobe_ticks >= 2*strobe_max)
	{
		strobe_ticks = 0;
		set_floods_brightness (strobe_brightness);
	}
}
static mode_t strobe_mode =
{
	/* update = */   strobe_update,
	
	/* on_click = */        strobe_to_next,
	/* on_double_click = */ extra_to_lock,
	/* on_hold = */         extra_to_off
};



/**********************************************************
 * Lock mode - ignore all clicks and holds, on double click
 * go to competition mode.  Flashes once every 10 seconds.
 **********************************************************/
#define LOCK_FLASH_PERIOD_MS     10000
#define LOCK_FLASH_PERIOD_TICKS  AS_TICKS(LOCK_FLASH_PERIOD_MS)
#define LOCK_FLASH_BRIGHTNESS    10
#define LOCK_FLASH_LENGTH_MS     100
#define LOCK_FLASH_LENGTH_TICKS  AS_TICKS(LOCK_FLASH_LENGTH_MS)

static mode_t* stay_locked (void)
{
	return &lock_mode;
}
static mode_t* lock_to_competition (void)
{
	competition_enter ();
	return &competition_mode;
}
static TYPE_TO_HOLD (LOCK_FLASH_PERIOD_TICKS) lock_tick_count = 0;
static void lock_enter (void)
{
	off_enter();
	lock_tick_count = 0;
}
static void lock_update (void)
{
	lock_tick_count++;
	if (lock_tick_count == (LOCK_FLASH_PERIOD_TICKS - LOCK_FLASH_LENGTH_TICKS))
	{
		set_floods_brightness (LOCK_FLASH_BRIGHTNESS);
	}
	else if (lock_tick_count >= LOCK_FLASH_PERIOD_TICKS)
	{
		lock_tick_count = 0;
		floods_off ();
	}
}
static mode_t lock_mode =
{
	/* update = */   lock_update,
	
	/* on_click = */        stay_locked,
	/* on_double_click = */ lock_to_competition,
	/* on_hold = */         stay_locked
};


/**********************************************************
 * Power-save functionality
 **********************************************************/

void disable_high_power (void)
{
	if (!in_powersave_mode)
	{
		/* Remember that this stuff has been done */
		in_powersave_mode = TRUE;
		
		/* Limit the flood brightnesses */
		levels = powersave_levels;
		max_levels = N_POWERSAVE_LEVELS;
		if (N_NORMAL_LEVELS > N_POWERSAVE_LEVELS
		        && brightness_index >= N_POWERSAVE_LEVELS)
			brightness_index = N_POWERSAVE_LEVELS - 1;
		if (floods_are_on ())
			set_floods_brightness (levels [brightness_index]);
		
		strobe_brightness = STROBE_BRIGHTNESS_POWERSAVE;
		
		/* Limit the spot brightness */
		spot_brightness = POWERSAVE_SPOT_BRIGHTNESS;
		if (spot_is_on ())
			set_spot_brightness (spot_brightness);
	}
}

void enable_high_power (void)
{
	if (in_powersave_mode)
	{
		/* Remember that this stuff has been done */
		in_powersave_mode = FALSE;
		
		/* Unlimit the flood brightness */
		levels = normal_levels;
		max_levels = N_NORMAL_LEVELS;
		if (N_POWERSAVE_LEVELS > N_NORMAL_LEVELS 
		        && brightness_index >= N_NORMAL_LEVELS)
		    brightness_index = N_NORMAL_LEVELS;
		
		strobe_brightness = STROBE_BRIGHTNESS_NORMAL;
		
		/* Unlimit the spot brightness */
		spot_brightness = NORMAL_SPOT_BRIGHTNESS;
		if (spot_is_on ())
			set_spot_brightness (spot_brightness);
	}
}

