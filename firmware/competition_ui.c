/** @file competition_ui.c
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Defines all the functions controlling the competition UI.
 */

#include "modes.h"
#include "main.h"
#include "lights.h"

#define N_NORMAL_LEVELS    5
#define N_POWERSAVE_LEVELS 4

#define NORMAL_SPOT_BRIGHTNESS     100
#define POWERSAVE_SPOT_BRIGHTNESS  50

typedef enum {SPOT, FLOOD} competition_mode_output_t;
static uint8_t normal_levels    [N_NORMAL_LEVELS]    = {1, 5, 15, 40, 100};
static uint8_t powersave_levels [N_POWERSAVE_LEVELS] = {1, 5, 15, 40};
static uint8_t *levels = normal_levels;
static uint8_t max_levels = N_NORMAL_LEVELS;

/* State variables */
competition_mode_output_t output = FLOOD;
static uint8_t brightness_index = 2; // Start with 15%

static uint8_t spot_brightness = NORMAL_SPOT_BRIGHTNESS;
static bool_t in_powersave_mode = FALSE;

static mode_t off_mode;
static mode_t competition_mode;
static mode_t programming_mode;

static void off_enter (void);
static void competition_enter (void);
static void programming_enter (void);

mode_t* ui_init (void)
{
	off_enter ();
	return &off_mode;
}

static void do_nothing (void) {}

/**********************************************************
 * Off mode - no lights on, Long press to enter 
 * competition mode.
 **********************************************************/
static void off_enter (void)
{
	floods_off ();
	spot_off ();
}

static mode_t* off_nop (void)
{
	return &off_mode;
}

static mode_t* off_to_competition_mode (void)
{
	competition_enter ();
	return &competition_mode;
}

static mode_t off_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        off_nop,
	/* on_double_click = */ off_nop,
	/* on_hold = */         off_to_competition_mode
};


/**********************************************************
 * Competition mode - toggles between flood and spotlight,
 * a double click goes to programming mode, and a hold
 * switches it off.
 **********************************************************/
static void competition_enter (void)
{
	if (output == FLOOD)
	{
		spot_off ();
		set_floods_brightness (levels [brightness_index]);
	}
	else /* output == SPOT */
	{
		floods_off ();
		set_spot_brightness (spot_brightness);
	}
}
static mode_t* competition_toggle (void)
{
	if (output == FLOOD)
	{
		output = SPOT;
		floods_off ();
		set_spot_brightness (spot_brightness);
	}
	else /* output == SPOT */
	{
		output = FLOOD;
		spot_off ();
		set_floods_brightness (levels [brightness_index]);
	}
	return &competition_mode;
}
static mode_t* competition_to_programming (void)
{
	programming_enter ();
	return &programming_mode;
}
static mode_t* competition_to_off (void)
{
	off_enter ();
	return &off_mode;
}
static mode_t competition_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        competition_toggle,
	/* on_double_click = */ competition_to_programming,
	/* on_hold = */         competition_to_off
};



/**********************************************************
 * Programming mode - alters the brightness of the floods
 * (so switches to flood mode if not there already), a
 * double click goes back to competition mode, and a hold
 * switches it off.
 **********************************************************/
static void increment_flood_brightness (void)
{
	brightness_index += 1;
	if (brightness_index >= max_levels)
		brightness_index = 0;
	set_floods_brightness (levels [brightness_index]);
}
static void programming_enter (void)
{
	output = FLOOD;
	spot_off ();
	set_floods_brightness (levels [brightness_index]);
}
static mode_t* programming_to_off (void)
{
	off_enter ();
	return &off_mode;
}
static mode_t* programming_increment (void)
{
	increment_flood_brightness ();
	return &programming_mode;
}
static mode_t* programming_to_competition (void)
{
	return &competition_mode;
}
static mode_t programming_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        programming_increment,
	/* on_double_click = */ programming_to_competition,
	/* on_hold = */         programming_to_off
};


/**********************************************************
 * Power-save functionality
 **********************************************************/

void disable_high_power (void)
{
	if (!in_powersave_mode)
	{
		/* Remember that this stuff has been done */
		in_powersave_mode = TRUE;
		
		/* Limit the flood brightnesses */
		levels = powersave_levels;
		max_levels = N_POWERSAVE_LEVELS;
		if (N_NORMAL_LEVELS > N_POWERSAVE_LEVELS
		        && brightness_index >= N_POWERSAVE_LEVELS)
			brightness_index = N_POWERSAVE_LEVELS - 1;
		if (floods_are_on ())
			set_floods_brightness (levels [brightness_index]);
		
		/* Limit the spot brightness */
		spot_brightness = POWERSAVE_SPOT_BRIGHTNESS;
		if (spot_is_on ())
			set_spot_brightness (spot_brightness);
	}
}

void enable_high_power (void)
{
	if (in_powersave_mode)
	{
		/* Remember that this stuff has been done */
		in_powersave_mode = FALSE;
		
		/* Unlimit the flood brightness */
		levels = normal_levels;
		max_levels = N_NORMAL_LEVELS;
		if (N_POWERSAVE_LEVELS > N_NORMAL_LEVELS 
		        && brightness_index >= N_NORMAL_LEVELS)
		    brightness_index = N_NORMAL_LEVELS;
		
		/* Unlimit the spot brightness */
		spot_brightness = NORMAL_SPOT_BRIGHTNESS;
		if (spot_is_on ())
			set_spot_brightness (spot_brightness);
	}
}

