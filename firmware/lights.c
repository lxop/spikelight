/** @file lights.c
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief
 */

#include "main.h"
#include "lights.h"
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/power.h>

static uint8_t spot_brightness   = 0;
static uint8_t floods_brightness = 0;
static bool_t changed = FALSE;
static volatile enum {FLOODS_OFF, FLOODS_QUARTER, FLOODS_HALF, FLOODS_FULL} flood_selection;


void lights_init (void)
{
	/* Ensure the outputs are off */
	clrbit (PORTB, PIN_SPOT);
	clrbit (PORTB, PIN_FLOOD1);
	clrbit (PORTB, PIN_FLOOD2);
	clrbit (PORTB, PIN_FLOOD_BOTH);
	
	/* Set pins as outputs */
	setbit (DDRB, PIN_SPOT);
	setbit (DDRB, PIN_FLOOD1);
	setbit (DDRB, PIN_FLOOD2);
	setbit (DDRB, PIN_FLOOD_BOTH);
	
	/* Set up the TCNT */
	// Want PWM freq ~100Hz, with say 100 brightness steps - ticks of 0-99.  So overflow value of 99
	OCR1C = 99;
	// Initialise the compare registers high so they don't trigger
	OCR1A = 0xFF;
	OCR1B = 0xFF;
	// Clear timer on compare match (with OCR1C)
	setbit (TCCR1, CTC1);
	// And PWM mode so the compare match causes an overflow interrupt
	setbit (TCCR1, PWM1A);
	// Enable timer overflow interrupt
	setbit (TIMSK, TOIE1);
	// Enable compare match interrupts
	setbit (TIMSK, OCIE1A);
	setbit (TIMSK, OCIE1B);
	
	/* Set the prescaler for TCNT clock ~10kHz:
	 * System clock of 1MHz : prescaler of 128 -> 7.8kHz
	 * System clock of 128kHz : prescaler of 16 -> 8kHz
	 */
#if F_CPU == 1000000
	setbit (TCCR1, CS13); // 1MHz main clock
#elif F_CPU == 128000
	setbit (TCCR1, CS12); // 128kHz main clock
	setbit (TCCR1, CS10); //
#else
	#error Unsupported clock frequency
#endif
	
	// Disable the counter until the lights are switched on
	power_timer1_disable ();
}

inline bool_t floods_are_on (void)
{
	return floods_brightness;
}

inline bool_t spot_is_on (void)
{
	return spot_brightness;
}

/* Sets up the compare registers and enables/disables the counter as
 * necessary to implement the current settings.  Call this after the
 * UI update function to implement any changes that were made.
 */
void lights_update (void)
{
	if (!changed)
		return;
	
	if (floods_are_on () | spot_is_on ())
	{
		power_timer1_enable ();
		/* Setup compare match values */
		/* Spot brightness is controlled by OCR1A */
		if (spot_brightness)
		{
			OCR1A = spot_brightness - 1;
		}
		else
		{
			OCR1A = 0xFF;
			clrbit (PORTB, PIN_SPOT);
		}
		
		/* Flood brightness is controlled by OCR1B */
		/* There are three pins controlling the floods. */
		/* The priorities of the interrupts is such that the "floods off"
		 * interrupt is lower priority than the "lights on" interrupt.
		 * The result of this is that when the "floods off" and 
		 * "lights on" interrupts occur at the same time (as happens when
		 * the level is set to 100), the LED is first switched on, then
		 * immediately switched off.  This can be avoided by simply 
		 * not hitting the "floods off" interrupt when the brightness is
		 * set to 100.
		 * The priorities are also such that the same problem does NOT
		 * affect the spot interrupts.  */
		if (floods_brightness > 50)
		{
			flood_selection = FLOODS_FULL;
			OCR1B = floods_brightness - 1;
		}
		else if (floods_brightness > 25)
		{
			flood_selection = FLOODS_HALF;
			OCR1B = 2*floods_brightness - 1;
		}
		else if (floods_brightness > 0)
		{
			flood_selection = FLOODS_QUARTER;
			OCR1B = 4*floods_brightness - 1;
		}
		else
		{
			flood_selection = FLOODS_OFF;
			OCR1B = 0xFF;
			clrbit (PORTB, PIN_FLOOD1);
			clrbit (PORTB, PIN_FLOOD2);
			clrbit (PORTB, PIN_FLOOD_BOTH);
		}
		if (OCR1B == 99)
			OCR1B += 1;
	}
	else
	{
		power_timer1_disable ();
		/* Ensure the lights aren't left on */
		clrbit (PORTB, PIN_SPOT);
		clrbit (PORTB, PIN_FLOOD1);
		clrbit (PORTB, PIN_FLOOD2);
		clrbit (PORTB, PIN_FLOOD_BOTH);
	}
	changed = FALSE;
}

void spot_off (void)
{
	spot_brightness = 0;
	changed = TRUE;
}

void floods_off (void)
{
	floods_brightness = 0;
	changed = TRUE;
}

/* level should be in [1..100].  There is no range checking: a value
 * of 0 will switch off the spot, and a value above 100 will have no
 * effect - it will just run at full power.
 */
void set_spot_brightness (uint8_t level)
{
	spot_brightness = level;
	changed = TRUE;
}

/* level should be in [1..100].  There is no range checking: a value
 * of 0 will switch off the floods, and a value above 100 will have no
 * effect - they will just run at full power.
 */
void set_floods_brightness (uint8_t level)
{
	floods_brightness = level;
	changed = TRUE;
}

/* On TCNT1 overflow, switch on any LEDs that are enabled */
ISR (TIM1_OVF_vect)
{
	if (OCR1A != 0xFF)
		setbit (PORTB, PIN_SPOT);
	
	switch (flood_selection)
	{
		case FLOODS_FULL:
			setbit (PORTB, PIN_FLOOD_BOTH);
		case FLOODS_HALF:
			setbit (PORTB, PIN_FLOOD1);
		case FLOODS_QUARTER:
			setbit (PORTB, PIN_FLOOD2);
		case FLOODS_OFF:
			break;
	}
}

/* On TCNT1 match A switch off spot */
ISR (TIM1_COMPA_vect, ISR_NAKED)
{
	clrbit (PORTB, PIN_SPOT);
	reti ();  // Necessary because of ISR_NAKED decorator
}

/* On TCNT1 match B switch off floods */
ISR (TIM1_COMPB_vect, ISR_NAKED)
{
	clrbit (PORTB, PIN_FLOOD1);
	clrbit (PORTB, PIN_FLOOD2);
	clrbit (PORTB, PIN_FLOOD_BOTH);
	reti ();  // Necessary because of ISR_NAKED decorator
}

