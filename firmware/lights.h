/** @file lights.h
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief
 */

#ifndef LIGHTS_H
#define LIGHTS_H

#include "main.h"

void lights_init (void);
void lights_update (void);

void spot_off (void);
void floods_off (void);

void set_spot_brightness (uint8_t level);
void set_floods_brightness (uint8_t level);

bool_t floods_are_on (void);
bool_t spot_is_on (void);

#endif /* LIGHTS_H */
