/** @file main.c
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Initialises the chip and performs high-level control.
 */

#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/power.h>

#include "main.h"
#include "lights.h"
#include "battery.h"
#include "button.h"
#include "modes.h"

void chip_init (void)
{
	/* Disable unneeded peripherals (just the USI) */
	power_usi_disable ();
	/* Disable the digital buffering on ADC0 (the reset pin, which is
	 * not connected to anything).  This will reduce power consumption
	 * a little. */
	setbit (DIDR0, ADC0D);
	/* Disable the analogue comparator (N.B. this is not the ADC) */
	setbit (ACSR, ACD);
	
	// Set TCNT0 prescaler and max value to fix the tick rate
	setbit (TCCR0A, WGM01); // Clear timer on compare match
	
	/* I should probably shift this conditional compilation stuff
	 * into another file to make this a bit cleaner. */
#if F_CPU == 1000000
	#if TICK_PERIOD_MS == 1
		setbit (TCCR0B, CS01); // Prescaler = 8 -> 125kHz
		OCR0A = 124; // Max value 124 (125 counts) - 1ms interrupt period
	#elif TICK_PERIOD_MS == 5
		setbit (TCCR0B, CS00); // Prescaler = 64 -> 15.625kHz
		setbit (TCCR0B, CS01); //
		OCR0A = 77;  // Max value 77 (78 counts) - ~5ms interrupt period
	#else
		#error "Unrecognised tick period"
	#endif
#elif F_CPU == 128000
	/* With this frequency, the tick period needs to be at least
	 * 3ms */
	#if TICK_PERIOD_MS == 5
		setbit (TCCR0B, CS01); // Prescaler = 8 -> 16kHz
		OCR0A = 79;  // Max value 79 (80 counts) - 5ms interrupt period
	#else
		#error "Unrecognised tick period"
	#endif
#endif
	
	// Enable TCNT0 "overflow" interrupt (actually compareA)
	setbit (TIMSK, OCIE0A);
	
	set_sleep_mode (SLEEP_MODE_IDLE);
}


void main (void)
{
	chip_init ();
	lights_init ();
	button_init ();
	battery_init ();
	modes_init ();
	
	sei ();
	
	sleep_enable ();
	/* Just keep sleeping - everything is done in ISRs. */
	while (1)
	{
		sleep_cpu ();
	}
}

// Look at whether it would be sensible to use the watchdog reset interrupt here instead
/* Perform the periodic update functions */
ISR (TIMER0_COMPA_vect, ISR_NOBLOCK)
{
	battery_tick ();
	mode_update ();
	lights_update ();
}
