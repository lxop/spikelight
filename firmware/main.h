/** @file main.h
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Constants controlling the operation of the spikelight and 
 *         useful macros for use throughout the code.
 */

#ifndef MAIN_H
#define MAIN_H

#include <avr/io.h>

/* F_CPU should be defined in the makefile when the fuses are set
 * that determine the clock that is used. */
#define TICK_PERIOD_MS 5         /* ms */

#define AS_TICKS(ms)  ((ms) / TICK_PERIOD_MS)

/* Pin assignments */
#define PIN_BUTTON      2
#define PIN_SPOT        0
#define PIN_FLOOD1      1
#define PIN_FLOOD2      4
#define PIN_FLOOD_BOTH  3

/* Bit manipulation macros */
#define bit(index)        (1<<(index))
#define setbit(port,pin)  (port |= bit(pin))
#define clrbit(port,pin)  (port &= ~(bit(pin)))
#define toggle(port,pin)  (port ^= bit(pin))
#define getbit(port,pin)  (port & bit(pin))

/* Simple max implementation.  Be sure not to use any expressions
 * with side-effects as arguments. */
#define MAX(a,b)  ((a) < (b) ? (b) : (a))

/* Mild trickery to get the smallest type that is large enough to
 * hold the value in the argument.  Useful to ensure that tick 
 * counters are sized automatically according to the tick period. */
#define CONCAT(x,y)   x ## y
#define DUMMY_ENUM_NAME(line)   CONCAT(dummy_enum_, line)
#define TYPE_TO_HOLD(top_value)  enum {DUMMY_ENUM_NAME(__LINE__) = (top_value)}

typedef enum {FALSE, TRUE} bool_t;

#endif /* MAIN_H */
