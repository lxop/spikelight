/** @file modes.c
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Implements the FSM framework that controls the operation 
 *         of the light.
 */

#include "modes.h"
#include "button.h"

static mode_t* current_mode;


void mode_update (void)
{
	click_t click = button_check ();
	switch (click)
	{
		case NO_CLICK:
			current_mode->update ();
			break;
		case SINGLE_CLICK:
			current_mode = current_mode->on_click ();
			break;
		case DOUBLE_CLICK:
			current_mode = current_mode->on_double_click ();
			break;
		case LONG_PRESS:
			current_mode = current_mode->on_hold ();
			break;
	}
}

void modes_init (void)
{
	/* Set up initial mode */
	current_mode = ui_init ();
}
