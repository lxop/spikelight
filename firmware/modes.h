/** @file modes.h
 *  @date August 2013
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Describes the FSM framework that controls the operation of 
 *         the light.
 */

#ifndef MODES_H
#define MODES_H

/* The mode system operates thus:  Every TICK_PERIOD_MS the update()
 * function of the current mode will be called, _unless_ the button
 * is pressed, in which case the appropriate callback will be called,
 * but _not_ the update function. */

typedef void (*void_func_t) (void);
typedef struct mode* (*callback_func_t) (void);

typedef struct mode
{
	void_func_t update;
	
	callback_func_t on_click;
	callback_func_t on_double_click;
	callback_func_t on_hold;
} mode_t;

/* Must be implemented by the UI */
extern mode_t* ui_init (void);

void modes_init (void);
void mode_update (void);


#endif /* MODES_H */
