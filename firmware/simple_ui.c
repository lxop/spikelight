/** @file simple_ui.c
 *  @date February 2014
 *  @author Alex Opie <amtopie@gmail.com>
 *  @brief Defines all the functions controlling the simple UI.
 */

#include "modes.h"
#include "main.h"
#include "lights.h"

#define N_LEVELS    3

#define NORMAL_SPOT_BRIGHTNESS     100
#define POWERSAVE_SPOT_BRIGHTNESS  50

#define NORMAL_MAX_FLOOD     100
#define POWERSAVE_MAX_FLOOD  40

static uint8_t levels [N_LEVELS] = {5, 25, NORMAL_MAX_FLOOD};
static uint8_t brightness_index = 0; // Start with 5%

static uint8_t spot_brightness = NORMAL_SPOT_BRIGHTNESS;
static bool_t in_powersave_mode = FALSE;

static mode_t off_mode;
static mode_t flood_mode;
static mode_t spot_mode;

static void off_enter (void);
static void flood_enter (void);
static void spot_enter (void);

mode_t* ui_init (void)
{
	off_enter ();
	return &off_mode;
}

static void do_nothing (void) {}

/**********************************************************
 * Off mode - no lights on, any click type to enter 
 * flood mode.
 **********************************************************/
static void off_enter (void)
{
	floods_off ();
	spot_off ();
}
static mode_t* off_to_flood_mode (void)
{
	flood_enter ();
	return &flood_mode;
}
static mode_t off_mode =
{
	/* update = */   do_nothing,
	
	/* on_click = */        off_to_flood_mode,
	/* on_double_click = */ off_to_flood_mode,
	/* on_hold = */         off_to_flood_mode
};



/**********************************************************
 * Flood mode - floods on, any click type to enter 
 * next brightest flood mode, except when at 100%, in 
 * which case go to spot mode.
 **********************************************************/
static void flood_enter (void)
{
	brightness_index = 0;
	spot_off ();
	set_floods_brightness (levels[0]);
}
static mode_t* flood_increment (void)
{
	brightness_index += 1;
	if (brightness_index >= N_LEVELS)
	{
		brightness_index = 0;
		spot_enter ();
		return &spot_mode;
	}
	set_floods_brightness (levels[brightness_index]);
	return &flood_mode;
}
static mode_t flood_mode = 
{
	/* update = */   do_nothing,
	
	/* on_click = */        flood_increment,
	/* on_double_click = */ flood_increment,
	/* on_hold = */         flood_increment
};


/**********************************************************
 * Spot mode - spot on, any click type to switch the 
 * light off.
 **********************************************************/
static void spot_enter (void)
{
	floods_off ();
	set_spot_brightness (spot_brightness);
}
static mode_t* switch_off (void)
{
	off_enter ();
	return &off_mode;
}
static mode_t spot_mode = 
{
	/* update = */   do_nothing,
	
	/* on_click = */        switch_off,
	/* on_double_click = */ switch_off,
	/* on_hold = */         switch_off
};



/**********************************************************
 * Power-save functionality
 **********************************************************/

void disable_high_power (void)
{
	if (!in_powersave_mode)
	{
		/* Remember that this stuff has been done */
		in_powersave_mode = TRUE;
		
		/* Limit the flood brightnesses */
		levels[N_LEVELS - 1] = POWERSAVE_MAX_FLOOD;
		if (floods_are_on ())
			set_floods_brightness (levels [brightness_index]);
		
		/* Limit the spot brightness */
		spot_brightness = POWERSAVE_SPOT_BRIGHTNESS;
		if (spot_is_on ())
			set_spot_brightness (spot_brightness);
	}
}

void enable_high_power (void)
{
	if (in_powersave_mode)
	{
		/* Remember that this stuff has been done */
		in_powersave_mode = FALSE;
		
		/* Unlimit the flood brightness */
		levels[N_LEVELS - 1] = NORMAL_MAX_FLOOD;
		if (floods_are_on ())
			set_floods_brightness (levels [brightness_index]);
		
		/* Unlimit the spot brightness */
		spot_brightness = NORMAL_SPOT_BRIGHTNESS;
		if (spot_is_on ())
			set_spot_brightness (spot_brightness);
	}
}

