import pdb

class SReg (object):
	def __init__ (self):
		self.C = 0
		self.Z = 0
		self.N = 0
		self.V = 0
		self.S = 0
		self.H = 0
		self.T = 0
		self.I = 0
		
		bits = [self.C, self.Z, self.N, self.V, 
		        self.S, self.H, self.T, self.I]
	
	def get_bit (self, bit):
		bits = [self.C, self.Z, self.N, self.V, 
		        self.S, self.H, self.T, self.I]
		return bits [bit]

class Register (object):
	def __init__ (self, value = 0):
		self.value = value
	
	def __repr__ (self):
		return "Register, value = %d" % self.value

class Machine (object):
	def __init__ (self, asm):
		### Initialise all the pieces:
		# Status register
		self.sreg = SReg ()
		# Program counter
		self.pc = 0
		# Registers
		self.registers = {}
		# Program code (assembly)
		self.asm = asm
		# Labels
		self.labels = {}
		# Bytecode
		self.bytecode = self.compile (asm)
	
	def initialise_registers (self, init_vals):
		# init_vals needs to be a dictionary, whose keys are register
		# names and values are the initial values of those registers
		for name in init_vals:
			self.registers[name] = Register(init_vals[name])
	
	def execute (self):
		# Grab the operation and operands from the bytecode,
		# and call the instruction
		self.clock_cycles = 0
		while (1):
			if self.clock_cycles >= 1000000:
				pdb.set_trace ()
			instruction = self.bytecode[self.pc]
			operation = instruction [0]
			operands = instruction [1:]
			if operation == self.op_ret:
				break
			
			operation (*operands)
	
	def __getitem__ (self, key):
		if key.startswith ('op_'):
			return self.__getattribute__(key)
		else:
			raise AttributeError (key)
	
	def compile (self, asm):
		bytecode = []
		for line in asm.splitlines ():
			instruction = line.split()
			if not len (instruction):
				# Empty line
				continue
			elif instruction[0].endswith(':'):
				# Label (remove colon)
				self.labels [instruction[0][:-1]] = len (bytecode)
			else:
				# Normal statement
				instruction[0] = self ["op_" + instruction[0]]
				for i in range (1, len (instruction)):
					instruction[i] = instruction[i].strip(',')
				bytecode.append (instruction)
		# Make sure the code ends with a ret command
		bytecode.append ([self.op_ret])
		return bytecode
	
	def get_register (self, name):
		# Simple wrapper around set_default, really
		reg = self.registers.get (name, None)
		if reg is None:
			reg = self.registers.setdefault (name, Register ())
		return reg
	
	def get_bit (self, value, bit):
		return (value & (1 << bit)) >> bit
	
	def set_bit (self, value, bit):
		return (value | (1 << bit))
	
	def op_ret (self):
		# Do nothing, this will be picked up as the end of the code
		pass
		
	def op_ror (self, Rd_name):
		Rd = self.get_register (Rd_name)
		sreg = self.sreg
		
		Rd0 = self.get_bit (Rd.value, 0)
		Rd.value = Rd.value >> 1
		if sreg.C:
			Rd.value = self.set_bit (Rd.value, 7)
		
		sreg.C = Rd0
		sreg.N = self.get_bit (Rd.value, 7)
		if Rd.value == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.V = sreg.N ^ sreg.C
		sreg.S = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_clr (self, Rd_name):
		Rd = self.get_register (Rd_name)
		sreg = self.sreg
		
		Rd.value = 0
		
		sreg.S = 0
		sreg.V = 0
		sreg.N = 0
		sreg.Z = 1
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_ldi (self, Rd_name, K):
		Rd = self.get_register (Rd_name)
		
		Rd.value = int (K)
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_lsl (self, Rd_name):
		Rd = self.get_register (Rd_name)
		sreg = self.sreg
		
		Rd7 = self.get_bit (Rd.value, 7)
		Rd3 = self.get_bit (Rd.value, 3)
		Rd.value = (Rd.value << 1) & 255
		
		sreg.C = Rd7
		sreg.H = Rd3
		sreg.N = self.get_bit (Rd.value, 7)
		if Rd.value == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.V = sreg.N ^ sreg.C
		sreg.S = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_rol (self, Rd_name):
		Rd = self.get_register (Rd_name)
		sreg = self.sreg
		
		Rd7 = self.get_bit (Rd.value, 7)
		Rd3 = self.get_bit (Rd.value, 3)
		Rd.value = ((Rd.value << 1) | sreg.C) & 255
		
		sreg.C = Rd7
		sreg.H = Rd3
		sreg.N = self.get_bit (Rd.value, 7)
		if Rd.value == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.V = sreg.N ^ sreg.C
		sreg.S = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_brcs (self, label):
		sreg = self.sreg
		
		if sreg.C:
			self.pc = self.labels [label]
			self.clock_cycles += 2
		else:
			self.pc += 1
			self.clock_cycles += 1
	
	def op_cp (self, Rd_name, Rr_name):
		Rd = self.get_register (Rd_name)
		Rr = self.get_register (Rr_name)
		sreg = self.sreg
		
		Rd7 = self.get_bit (Rd.value, 7)
		Rd3 = self.get_bit (Rd.value, 3)
		Rr7 = self.get_bit (Rr.value, 7)
		Rr3 = self.get_bit (Rr.value, 3)
		R = (Rd.value - Rr.value) & 255
		R7 = self.get_bit (R, 7)
		R3 = self.get_bit (R, 3)
		
		sreg.N = self.get_bit (R, 7)
		if R == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.C = ((~Rd7 & Rr7) | (Rr7 & R7) | (R7 & ~Rd7)) & 1
		sreg.V = ((Rd7 & ~Rr7 & ~R7) | (~Rd7 & Rr7 & R7)) & 1
		sreg.S = sreg.N ^ sreg.V
		sreg.H = ((~Rd3 & Rr3) | (Rr3 & R3) | (R3 & ~Rd3)) & 1
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_brlo (self, label):
		sreg = self.sreg
		
		if sreg.C:
			self.pc = self.labels [label]
			self.clock_cycles += 2
		else:
			self.pc += 1
			self.clock_cycles += 1
	
	def op_brbc (self, bit, label):
		sreg = self.sreg
		bit = int (bit)
		
		if sreg.get_bit (bit) == 0:
			self.pc = self.labels [label]
			self.clock_cycles += 2
		else:
			self.pc += 1
			self.clock_cycles += 1
	
	def op_rjmp (self, label):
		self.pc = self.labels [label]
		self.clock_cycles += 2
	
	def op_sub (self, Rd_name, Rr_name):
		Rd = self.get_register (Rd_name)
		Rr = self.get_register (Rr_name)
		sreg = self.sreg
		
		R = (Rd.value - Rr.value) & 255
		Rd7 = self.get_bit (Rd.value, 7)
		Rd3 = self.get_bit (Rd.value, 3)
		Rr7 = self.get_bit (Rr.value, 7)
		Rr3 = self.get_bit (Rr.value, 3)
		R7 = self.get_bit (R, 7)
		R3 = self.get_bit (R, 3)
		Rd.value = R
		
		sreg.H = ((~Rd3 & Rr3) | (Rr3 & R3) | (R3 & ~Rd3)) & 1
		sreg.V = ((Rd7 & ~Rr7 & ~R7) | (~Rd7 & Rr7 & R7)) & 1
		sreg.N = R7
		sreg.C = ((~Rd7 & Rr7) | (Rr7 & R7) | (R7 & ~Rd7)) & 1
		if R == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.S = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_sbc (self, Rd_name, Rr_name):
		Rd = self.get_register (Rd_name)
		Rr = self.get_register (Rr_name)
		sreg = self.sreg
		
		R = (Rd.value - Rr.value - sreg.C) & 255
		Rd7 = self.get_bit (Rd.value, 7)
		Rd3 = self.get_bit (Rd.value, 3)
		Rr7 = self.get_bit (Rr.value, 7)
		Rr3 = self.get_bit (Rr.value, 3)
		R7 = self.get_bit (R, 7)
		R3 = self.get_bit (R, 3)
		Rd.value = R
		
		sreg.H = ((~Rd3 & Rr3) | (Rr3 & R3) | (R3 & ~Rd3)) & 1
		sreg.V = ((Rd7 & ~Rr7 & ~R7) | (~Rd7 & Rr7 & R7)) & 1
		sreg.N = R7
		sreg.C = ((~Rd7 & Rr7) | (Rr7 & R7) | (R7 & ~Rd7)) & 1
		if (R == 0) and (sreg.Z == 1):
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.S = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_or (self, Rd_name, Rr_name):
		Rd = self.get_register (Rd_name)
		Rr = self.get_register (Rr_name)
		sreg = self.sreg
		
		Rd.value = Rd.value | Rr.value
		
		sreg.V = 0
		sreg.N = self.get_bit (Rd.value, 7)
		if Rd.value == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.Z = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	def op_lsr (self, Rd_name):
		Rd = self.get_register (Rd_name)
		sreg = self.sreg
		
		Rd0 = self.get_bit (Rd.value, 0)
		Rd.value = Rd.value >> 1
		
		sreg.C = Rd0
		sreg.N = 0
		if Rd.value == 0:
			sreg.Z = 1
		else:
			sreg.Z = 0
		sreg.V = sreg.N ^ sreg.C
		sreg.S = sreg.N ^ sreg.V
		
		self.pc += 1
		self.clock_cycles += 1
	
	
