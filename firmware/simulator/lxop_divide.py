import avr_sim

dividend = 65535
divisor = 304

def get_byte (value, which):
	mask = 255 << (8*which)
	return (value & mask) >> (8*which)

def build_value (*bytes):
	value = 0
	for offset, byte in enumerate (bytes):
		value |= (byte << (8*offset))
	return value

asm = """
clr %D1
clr %C1
clr %B1
ldi %A1, 1
clr %D0
clr %C0
clr %B0
clr %A0
 
start_%=:
lsl %A3
rol %B3
rol %C3
rol %D3
brcs overflow_%=
lsl %A1
rol %B1
rol %C1
rol %D1
 
cp %D2, %D3
brlo divide_%=  
brbc 1, start_%=
cp %C2, %C3
brlo divide_%=  
brbc 1, start_%=
cp %B2, %B3
brlo divide_%=  
brbc 1, start_%=
cp %A2, %A3
brlo divide_%=  
brbc 1, start_%=
rjmp divide_%=
 
overflow_%=:
ror %D3
ror %C3
ror %B3
ror %A3
 
divide_%=:
cp %D2, %D3
brlo shift_%=
brbc 1, subt_%= 
cp %C2, %C3
brlo shift_%=
brbc 1, subt_%= 
cp %B2, %B3
brlo shift_%=
brbc 1, subt_%= 
cp %A2, %A3
brlo shift_%=

subt_%=:
sub %A2, %A3
sbc %B2, %B3
sbc %C2, %C3
sbc %D2, %D3
or  %A0, %A1
or  %B0, %B1
or  %C0, %C1
or  %D0, %D1
 
shift_%=:
lsr %D1
ror %C1
ror %B1
ror %A1
brcs end_%=
lsr %D3
ror %C3
ror %B3
ror %A3
rjmp divide_%=
 
end_%=:
"""

regs = {"%A2" : get_byte (dividend, 0),
        "%B2" : get_byte (dividend, 1),
        "%C2" : get_byte (dividend, 2),
        "%D2" : get_byte (dividend, 3),
        
        "%A3" : get_byte (divisor, 0),
        "%B3" : get_byte (divisor, 1),
        "%C3" : get_byte (divisor, 2),
        "%D3" : get_byte (divisor, 3)}

sim = avr_sim.Machine (asm)
sim.initialise_registers (regs)
sim.execute ()
final_regs = sim.registers
quotient = build_value (final_regs["%A0"].value,
                        final_regs["%B0"].value,
                        final_regs["%C0"].value,
                        final_regs["%D0"].value)
remainder = build_value (final_regs["%A2"].value,
                         final_regs["%B2"].value,
                         final_regs["%C2"].value,
                         final_regs["%D2"].value)
print "%d / %d = %d, remainder %d  -> this took %d clock cycles." %(
	dividend, divisor, quotient, remainder, sim.clock_cycles)

